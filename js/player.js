function player() {
	
	var apiUrl = 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=' + playlist.playlist + '&key=' + playlist.apiKey;
	
	$.getJSON(apiUrl, function(result) {
		var embed = result.items[0].snippet.resourceId.videoId;
		var title = result.items[0].snippet.title;
		$('.playlist-container > h2').text(title);
		$('.embed-container').html('<iframe src="https://www.youtube.com/embed/'+embed+'" frameborder="0" allowfullscreen></iframe>');
		$.each(result.items, function(key, item) {
			var title = item.snippet.title;
			var id = item.snippet.resourceId.videoId;
			var thumb = item.snippet.thumbnails.medium.url;
			playlist.container.append('<li class="playlist-item"><a class="playlist-link" data-id="'+id+'" href="#"><img src="'+thumb+'" /><h5>'+title+'</h5></a></li>');
		});
		$('.playlist-item:first-of-type').addClass('active');
	});
	
	$(document).on('click', '.playlist-link', function(e) {
		e.preventDefault();
		embed = $(this).data('id');
		title = $(this).text();
		$('.playlist-item').removeClass('active');
		$(this).closest('.playlist-item').addClass('active');
		$('.playlist-container > h2').text(title)
		$('.embed-container').empty().html('<iframe src="https://www.youtube.com/embed/'+embed+'" frameborder="0" allowfullscreen></iframe>');
	});
	
	$(document).on('click', '.playlist-container .prev', function() {
		$(this).closest('.playlist-container').find('.playlist-gallery > .playlist-item:first').appendTo(".playlist-gallery");
	});
	
	$(document).on('click', '.playlist-container .next', function() {
		$(this).closest('.playlist-container').find('.playlist-gallery > .playlist-item:last').prependTo(".playlist-gallery");
	});
	
}
$('.modal').on('shown.bs.modal', function (e) {
	var id = $(this).find('.modal-dialog').find('.playlist-gallery').attr('id');
	playlist = {
		playlist: id,
		apiKey: 'AIzaSyBIyD8ZOQGJdt2k_1rs_uuQ_HCEeSWi5Kg',
		container: $('#'+id)
	}
	player();
});

$('.modal').on('hide.bs.modal', function (e) {
	$('.embed-container, .playlist-gallery, .playlist-container > h2').empty();
});