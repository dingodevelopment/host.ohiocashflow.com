// =======================
// MOBILE & DESKTOP TWEAKS
// =======================
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows();
    }
};

if (!isMobile.any()) {
    $('a[href^="tel:"]')
        .bind('click', false)
        .css({ color: 'inherit', cursor: 'default', 'background-color': 'inherit' });
    var bottomSpacing = $('footer').outerHeight() + 60;
    $('#sticker').sticky({
        topSpacing: 145,
        bottomSpacing: bottomSpacing
    });
    $('#sticker2').sticky({
        topSpacing: 145,
        bottomSpacing: bottomSpacing
    });
    $('#sticker3').sticky({
        topSpacing: 145,
        bottomSpacing: bottomSpacing
    });
}

if (isMobile.any()) {
    $('#sticker').appendTo('.main');
    $('#sticker2').appendTo('.main');
    $('#sticker3').appendTo('.main');
}

// =======================
// FULLSCREEN MODALS
// =======================

$('.modal-fullscreen').on('show.bs.modal', function() {
    setTimeout(function() {
        $('.modal-backdrop').addClass('modal-backdrop-fullscreen');
    }, 0);
});
$('.modal-fullscreen').on('hidden.bs.modal', function() {
    $('.modal-backdrop').addClass('modal-backdrop-fullscreen');
});

// =======================
// MISC ON READY & LOAD
// =======================

$(document).ready(function() {
    var navHeight = 0;
    var innerHeight = 0;
    $('#year').text(new Date().getFullYear());
    $('nav').each(function() {
        navHeight += $(this).outerHeight();
    });
    $('.jumbotron.home-jumbotron > div').each(function() {
        innerHeight += $(this).height();
    });
    $('#content').css('margin-top', navHeight + 'px');
    $('.jumbotron.home-jumbotron').css({
        padding: ($(window).height() - (navHeight + innerHeight)) / 2 + 'px 0'
    });
});

$(window).resize(function() {
    var navHeight = 0;
    var innerHeight = 0;
    $('nav').each(function() {
        navHeight += $(this).outerHeight();
    });
    $('.jumbotron.home-jumbotron > div').each(function() {
        innerHeight += $(this).height();
    });
    $('#content').css('margin-top', navHeight + 'px');
    $('.jumbotron.home-jumbotron').css({
        padding: ($(window).height() - (navHeight + innerHeight)) / 2 + 'px 0'
    });
});

// =======================
// LANDING PAGE MODAL
// =======================
var parser = document.createElement('a');
parser.href = document.referrer;
var isExternallyReferred = document.referrer.length > 0 && parser.hostname !== window.location.hostname;

if (
    (isExternallyReferred && /videos/.test(window.location.href)) ||
    /media/.test(window.location.href) ||
    /post/.test(window.location.href) ||
    /properties/.test(window.location.href)
) {
    $('#newsletter').modal('show');
}

// =======================
// STOP YOUTUBE ON MODAL X
// =======================
$('.modal-backdrop, .modal .close').on('click', function() {
    $('.embed-container iframe').each(function() {
        var src = $(this)
            .closest('.embed-container')
            .find('iframe')
            .attr('src');
        $(this)
            .closest('.embed-container')
            .find('iframe')
            .attr('src', '');
        $(this)
            .closest('.embed-container')
            .find('iframe')
            .attr('src', src);
    });
});

// =======================
// CARD TOGGLE
// =======================
$('.card-toggle').on('click', function() {
    $('.card-toggle')
        .not(this)
        .parent()
        .siblings('.card-text')
        .hide(250)
        .siblings('.card-img')
        .show(250);
    $(this)
        .parent()
        .siblings('.card-img, .card-text')
        .toggle(250);
});

function myfun() {
    // Write your business logic here
    console.log('hello');
}

// =======================
// LIGHTBOX GALLERY
// =======================

function loopGalleryPrev() {
    $(".gallery-item:first").appendTo(".gallery");
    $(".gallery-item:last").hide().fadeIn(1000);
  }
  
  function loopGalleryNext() {
    $(".gallery-item:last").prependTo(".gallery");
    $(".gallery-item:first").hide().fadeIn(1000);
  }
  
  var loopDir = loopGalleryNext;
  var interval;
  
//   $(document).ready(function() {
//     interval = setInterval(loopDir, 4000);
//   })
  
  $(document).on('click', '.gallery-container > .prev', function() {
    loopGalleryPrev();
    interval = clearInterval(interval);
    loopDir = loopGalleryPrev;
    interval = setInterval(loopDir, 4000);
  });
  
  $(document).on('click', '.gallery-container > .next', function() {
    loopGalleryNext();
    interval = clearInterval(interval);
    loopDir = loopGalleryNext;
    interval = setInterval(loopDir, 4000);
  });
  
  var next = '<div class="next"><span class="fa fa-arrow-right"></span></div>';
  var prev = '<div class="prev"><span class="fa fa-arrow-left"></span></div>';
  var dismiss = '<div class="dismiss"><span class="fa fa-times"></span></div>';
  var slide;
  
  $(document).on('click', '.gallery-link', function(e) {
    e.preventDefault();
    interval = clearInterval(interval);
    slide = $(this).closest(".gallery-item");
    $(slide).addClass("shown");
    $('.shown').append(dismiss);
    if($('.shown').prev().length) {
      $('.shown').append(prev);
    }
    if($('.shown').next().length) {
      $('.shown').append(next);
    }
  })
  
  $(document).on('click', '.gallery-item > .prev', function() {
    slide = $(this).closest(".gallery-item");
    $(this).closest(".gallery-item").children(".prev, .next, .dismiss").remove();
    if(slide.prev().length) {
      $(slide).removeClass("shown").closest(".gallery-item").prev().addClass("shown");
    }
    $('.shown').append(dismiss);
    if($('.shown').prev().length) {
      $('.shown').append(prev);
    }
    if($('.shown').next().length) {
      $('.shown').append(next);
    }
  });
  
  $(document).on('click', '.gallery-item > .next', function() {
    slide = $(this).closest(".gallery-item");
    $(this).closest(".gallery-item").children(".prev, .next, .dismiss").remove();
    if(slide.next().length) {
      $(slide).removeClass("shown").closest(".gallery-item").next().addClass("shown");
    }
    $('.shown').append(dismiss);
    if($('.shown').prev().length) {
      $('.shown').append(prev);
    }
    if($('.shown').next().length) {
      $('.shown').append(next);
    }
  });
  
  $(document).on('click', '.gallery-item > .dismiss', function() {
    $(this).closest(".gallery-item").removeClass("shown");
    $(this).closest(".gallery-item").children(".prev, .next, .dismiss").remove();
    interval = setInterval(loopDir, 4000);
  });
